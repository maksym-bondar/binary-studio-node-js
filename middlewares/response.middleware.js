const responseMiddleware = (req, res, next) => {
  try {
    if (req) {
      next();
    }
  } catch (e) {
    res.status(400).json({ error: true, message: e.message });
  }
};

exports.responseMiddleware = responseMiddleware;
