const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  getFighters = () => FighterRepository.getAll();
  getFighter = (id) => this.search({ id });
  createFighter = (fighter) => {
    if (!fighter.hasOwnProperty("health")) {
      return FighterRepository.create({ ...fighter, health: 100 });
    }
    return FighterRepository.create(fighter);
  };
  updateFighter = (id, fighter) => FighterRepository.update(id, fighter);
  deleteFighter = (id) => FighterRepository.delete(id);

  search(search) {
    const fighterSearch = FighterRepository.getOne(search);

    if (!fighterSearch) {
      return null;
    }
    return fighterSearch;
  }
}

module.exports = new FighterService();
